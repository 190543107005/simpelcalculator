package com.example.sed;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
EditText e1,e2;
Button ADD,SUB,MUL,DIV;
TextView t,t1,t2,t3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        e1=findViewById(R.id.value1);
        e2=findViewById(R.id.value2);
        ADD=findViewById(R.id.button);
        SUB=findViewById(R.id.button1);
        MUL=findViewById(R.id.button2);
        DIV=findViewById(R.id.button3);
        t=findViewById(R.id.ettextview);
        t1=findViewById(R.id.ettextview1);
        t2=findViewById(R.id.ettextview2);
        t3=findViewById(R.id.ettextview3);
        ADD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val1=Integer.parseInt(e1.getText().toString());
                int val2=Integer.parseInt(e2.getText().toString());
                int temp=val1+val2;
                t.setText("Addition="+temp);
            }
        });
        SUB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val1=Integer.parseInt(e1.getText().toString());
                int val2=Integer.parseInt(e2.getText().toString());
                int temp=val1-val2;
                t1.setText("Subtraction="+temp);
            }
        });
        MUL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val1=Integer.parseInt(e1.getText().toString());
                int val2=Integer.parseInt(e2.getText().toString());
                int temp=val1*val2;
                t2.setText("Multiplication="+temp);
            }
        });
        DIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val1=Integer.parseInt(e1.getText().toString());
                int val2=Integer.parseInt(e2.getText().toString());
                int temp=val1/val2;
                t3.setText("Division="+temp);
            }
        });
    }
}